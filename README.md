# Jarvis Exchange Artwork

[![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

## Contents

* [`backgrounds/`](./backgrounds/)
  * [`garden/`](./backgrounds/garden)
    * [`light-mode-background.png`](./backgrounds/garden/light-mode-background.png)
    * [`dark-mode-background.png`](./backgrounds/garden/dark-mode-background.png)
    * [`night-mode-background.png`](./backgrounds/garden/night-mode-background.png)
    * [`sculpture-girl.svg`](./backgrounds/garden/sculpture-girl.svg)

## LICENSE

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: https://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-green.svg
